### Natural Numbers Game

This repository contains my solutions to the [Lean Intro to Logic game](https://adam.math.hhu.de/#/g/trequetrum/lean4game-logic) on Lean 4.
